//
//  SingletonsTestTests.m
//  SingletonsTestTests
//
//  Created by Rafal Augustyniak on 21.06.2014.
//  Copyright (c) 2014 Rafal Augustyniak. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface SingletonsTestTests : XCTestCase

@end

@implementation SingletonsTestTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample
{
    XCTFail(@"No implementation for \"%s\"", __PRETTY_FUNCTION__);
}

@end
