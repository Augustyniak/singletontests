//
//  RAAppDelegate.h
//  SingletonsTest
//
//  Created by Rafal Augustyniak on 21.06.2014.
//  Copyright (c) 2014 Rafal Augustyniak. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RAAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
