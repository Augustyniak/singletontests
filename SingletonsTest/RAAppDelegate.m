//
//  RAAppDelegate.m
//  SingletonsTest
//
//  Created by Rafal Augustyniak on 21.06.2014.
//  Copyright (c) 2014 Rafal Augustyniak. All rights reserved.
//

#import "RAAppDelegate.h"
#import <objc/objc-runtime.h>

@implementation RAAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
  RunTests();
    // Override point for customization after application launch.
  return YES;
}


static void RunTests()
{
  int numberOfClasses = objc_getClassList(NULL, 0);
  Class *classList = (__unsafe_unretained Class *)calloc(numberOfClasses,sizeof(Class));
  numberOfClasses = objc_getClassList(classList, numberOfClasses);
  
  NSUInteger counter = 0, successes = 0, numberOFTheSameObjects = 0;
  for (int classIndex = 0; classIndex < numberOfClasses; classIndex++)
  {
    Class class = classList[classIndex];
    unsigned int count = 0;
    Method *methodList = class_copyMethodList(object_getClass(class), &count);
    for (unsigned int i = 0; i < count; i ++) {
      
      NSString *methodName = [NSString stringWithCString:sel_getName(method_getName(methodList[i])) encoding:NSUTF8StringEncoding];
      unsigned int numberOfArguments = method_getNumberOfArguments(methodList[i]);
      
      if ([methodName rangeOfString:@"shared"].location == 0 && numberOfArguments == 2) {
        
        BOOL successfulInitialization = NO, sameInstances = NO;
        CheckClass(class, &successfulInitialization, &sameInstances);
        successes += successfulInitialization ? 1 : 0;
        numberOFTheSameObjects += sameInstances ? 1 : 0;
        counter++;
      }
    }
    
    if (methodList != NULL) {
      free(methodList);
    }
  }
  
  if (classList != NULL) {
    free(classList);
  }
  
  NSLog(@"Results \n Classes with shared... method name: %@ \n Successful alloc+init: %@ \n Returned the same object in next 2 calls: %@", [@(counter) stringValue], [@(successes) stringValue], [@(numberOFTheSameObjects) stringValue]);
}

void CheckClass(Class class, BOOL *success, BOOL *sameObjects)
{
  *success = *sameObjects = NO;
  id firstObject = nil, secondObject = nil;
  
  @try {
    
    NSArray *brokenClasses = @[@"WebView", @"MSASPersonInfoManager", @"TUTelephonyAudioController"];
    
    if ([brokenClasses containsObject:NSStringFromClass(class)]) {
      *success = NO;
      return;
    }
    
    firstObject = [[class alloc] init];
    secondObject = [[class alloc] init];
    
    if (!firstObject || !secondObject) {
      *success = NO;
      return;
    }
    
    RetainObject(firstObject);
    RetainObject(secondObject);
    //Some classes must be retained here.
    //When some classes initialization fails retain counter is unbalaned and
    //ARC tries to release object with retain count equal to 0.
    //This workaround works but it createas an obvious memmory leak.
    
    if (firstObject == secondObject) {
      *sameObjects = YES;
    }
  }
  
  @catch (NSException *exception) {
    
    NSLog(@"Class %@ cannot be initialized.", NSStringFromClass(class));
    *success = NO;
    return;
  }
  
  *success = YES;
}

static void RetainObject(id object)
{
  SEL retain = sel_getUid("retain");
  IMP imp = class_getMethodImplementation(object_getClass(object), retain);
  //According to some sources +class method on NSObject sometimes returns wrong
  //class name. In this way Apple tries to hide some internal implementation details.
  
  ((id (*)(CFTypeRef, SEL))imp)((__bridge CFTypeRef) object, retain);
}

@end
