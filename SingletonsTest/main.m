//
//  main.m
//  SingletonsTest
//
//  Created by Rafal Augustyniak on 21.06.2014.
//  Copyright (c) 2014 Rafal Augustyniak. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "RAAppDelegate.h"

int main(int argc, char * argv[])
{
  @autoreleasepool {
      return UIApplicationMain(argc, argv, nil, NSStringFromClass([RAAppDelegate class]));
  }
}
