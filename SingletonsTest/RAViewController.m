//
//  RAViewController.m
//  SingletonsTest
//
//  Created by Rafal Augustyniak on 21.06.2014.
//  Copyright (c) 2014 Rafal Augustyniak. All rights reserved.
//

#import "RAViewController.h"

@interface RAViewController ()

@end

@implementation RAViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
